package com.example.a123rftest.canvas

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.View
import com.bumptech.glide.Glide
import com.example.a123rftest.R
import jp.co.cyberagent.android.gpuimage.GPUImage
import jp.co.cyberagent.android.gpuimage.filter.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.MainScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.math.floor

class MyCustomView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : View(context, attrs, defStyleAttr) {
    // persistent values (for screen rotating)
    companion object {
        var rowColumnCount = 2 // e.g. 2 = 2x2, 3 = 3x3...
        var selectedBoxIndex = -99
        var gpuImgFilterIndex = -99
    }

    // non-persistent (need to resize img when rotate)
    private var imgBitmap: Bitmap? = null
    private var gpuImg: GPUImage? = null

    // square boxes styling
    private val boxPaint = Paint().apply {
        setLayerType(LAYER_TYPE_HARDWARE, this)
        style = Paint.Style.STROKE
        strokeWidth = 3f
    }

    private val imgScope = MainScope()
    private fun loadBitmapFrom4k() {
        // only need to load img once (will reload when rotate)
        if (imgBitmap != null) return

        imgScope.launch {
            withContext(Dispatchers.IO) {
                // load 4k img into bitmap
                imgBitmap = Glide.with(context)
                    .asBitmap()
                    .load(R.drawable.img_4k)
                    .override(boxWidth.toInt(), boxWidth.toInt())
                    .submit()
                    .get()

                Log.d(
                    "JAY_LOG",
                    "MyCustomView: loadBitmapFrom4k imgBitmapByte = ${imgBitmap?.byteCount}"
                )

                // set bitmap img to GPUimg for later processing
                if (gpuImg == null) {
                    gpuImg = GPUImage(context)
                }
                gpuImg?.apply {
                    setImage(imgBitmap)

                    // apply filter if got
                    if (gpuImgFilterIndex != -99) {
                        setFilter(gpuImgFilterList[gpuImgFilterIndex])
                        imgBitmap = gpuImg?.bitmapWithFilterApplied
                    }
                }
            }.also {
                Log.d("JAY_LOG", "MyCustomView: loadBitmapFrom4k ")
                invalidate()
            }
        }
    }

    //=== onclick funcs
    fun addNumberOfBoxes() {
        if (rowColumnCount >= 4)
            rowColumnCount = 2
        else
            rowColumnCount++

        // bring img into view if selected index was more than new number of grids
        if (selectedBoxIndex > rowColumnCount * rowColumnCount - 1) selectedBoxIndex =
            rowColumnCount * rowColumnCount - 1

        // get new box width based on new number of grids - to resize bitmap correctly
        getBoxWidth()

        // reset img size (but retain filter)
        imgBitmap = null
        loadBitmapFrom4k()
    }


    fun resetImgEffectsOnClick() {
        // clear selected index
        selectedBoxIndex = -99

        // reset img size AND filters
        imgBitmap = null
        gpuImgFilterIndex = -99
        loadBitmapFrom4k()
    }

    // load img onto touched box on click
    // TODO: can use event position to determine which box to populate img
    private var gpuImgFilterList =
        listOf(
            GPUImageSepiaToneFilter(),
            GPUImageAlphaBlendFilter(),
            GPUImageChromaKeyBlendFilter(),
            GPUImageLaplacianFilter(),
            GPUImageDifferenceBlendFilter(),
            GPUImageLuminanceFilter()
        )

    fun applyImgOnClick() {
        // select a random box if not already selected
        if (selectedBoxIndex == -99) {
            selectedBoxIndex = (0 until rowColumnCount * rowColumnCount).random()

        } else {
            // already selected - apply filter
            gpuImgFilterIndex = (gpuImgFilterList.indices).random()
            gpuImg?.setFilter(gpuImgFilterList[gpuImgFilterIndex])
            imgBitmap = gpuImg?.bitmapWithFilterApplied
        }

        invalidate()
    }

    private val singlePadding = 50F
    private val imgRect = RectF()
    private var boxWidth = 0F
    private var boxWidthAndSinglePadding = 0F
    private fun getBoxWidth() {
        boxWidth = (width - singlePadding * (rowColumnCount + 1)) / rowColumnCount
    }

    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)

        getBoxWidth()
        boxWidthAndSinglePadding = singlePadding + boxWidth

        // load bitmap only after getting box width
        loadBitmapFrom4k()

        // populate boxes in grid
        for (i in 0 until rowColumnCount * rowColumnCount) {
            val currentRow = getRow(i)
            val currentColumn = getColumn(i, currentRow)

            val leftF =
                if (isFirstColumn(currentColumn)) singlePadding else singlePadding + currentColumn * boxWidthAndSinglePadding
            val topF =
                if (isFirstRow(currentRow)) singlePadding else singlePadding + currentRow * boxWidthAndSinglePadding
            val rightF = leftF + boxWidth
            val bottomF = topF + boxWidth

            // set img to box if got
            if (selectedBoxIndex == i) {
                imgRect.apply {
                    left = leftF
                    top = topF
                    right = rightF
                    bottom = bottomF
                }

                imgBitmap?.let {
                    canvas?.drawBitmap(it, null, imgRect, boxPaint)
                }

            } else { // no selected index, just load empty box
                canvas?.drawRect(leftF, topF, leftF + boxWidth, topF + boxWidth, boxPaint)
            }
        }
    }

    // get position (in grid)
    private fun getRow(currentIndex: Int) = floor(currentIndex.toDouble() / rowColumnCount).toInt()
    private fun getColumn(currentIndex: Int, row: Int) = (currentIndex - row * rowColumnCount)

    // functions for checking if row/col is first or not
    private fun isFirstRow(row: Int) = row == 0
    private fun isFirstColumn(column: Int) = column == 0
}