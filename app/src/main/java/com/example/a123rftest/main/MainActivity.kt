package com.example.a123rftest.main

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.example.a123rftest.R
import com.example.a123rftest.canvas.MyCustomView
import com.example.a123rftest.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    // constant strings for bundle keys (rotation)
    private val NUMBER_OF_GRIDS = "NUMBER_OF_GRIDS"
    private val SELECTED_BOX_INDEX = "SELECTED_BOX_INDEX"
    private val GPU_IMG_FILTER = "GPU_IMG_FILTER"

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        setOnClickListeners()
    }

    // save/restore instance for rotation
    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)

        outState.putInt(NUMBER_OF_GRIDS, MyCustomView.rowColumnCount)
        outState.putInt(SELECTED_BOX_INDEX, MyCustomView.selectedBoxIndex)
        outState.putInt(GPU_IMG_FILTER, MyCustomView.gpuImgFilterIndex)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)

        MyCustomView.rowColumnCount = savedInstanceState.getInt(NUMBER_OF_GRIDS)
        MyCustomView.selectedBoxIndex = savedInstanceState.getInt(SELECTED_BOX_INDEX)
    }

    private fun setOnClickListeners() {
        binding.fabPlus.setOnClickListener {
            binding.myCustomView.addNumberOfBoxes()
        }

        binding.fabGallery.setOnClickListener {
            binding.myCustomView.applyImgOnClick()
        }

        binding.fabMinus.setOnClickListener {
            binding.myCustomView.resetImgEffectsOnClick()
        }
    }
}